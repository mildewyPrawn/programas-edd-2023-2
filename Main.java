import java.util.ArrayList;

public class Main {
    
    public static ArrayList<Animal> suma(ArrayList<? extends Animal> list) {
        ArrayList<Animal> animals = new ArrayList<>();
        for (Animal an : list) {
            an.imprimir();
            animals.add(an);
        }

        Perro perrito1 = new Perro();
        animals.add(new Perro());
        animals.add(perrito1);
        return animals;
    }

    public static void main(String[] args) {
        Pato p1 = new Pato();
        Pato p2 = new Pato();
        Pato p3 = new Pato();

        ArrayList<Pato> patos = new ArrayList<>();
        patos.add(p1);
        patos.add(p2);
        patos.add(p3);

        System.out.println(suma(patos));
    }
    
}
